<?php

/**
* Classe responsável por interagir com o Web Service do Correios
*
* PHP 5.3+
*
* @author Arthur Duarte <arthur@aduarte.net>
* @category Library
*
* Referência dos possíveis erros encaminhado pelo Web Service do Correios.
* Lembrando que em caso de falha, o identificador do erro bem como a mensagem do erro virá junto ao resultado.
* A lista abaixo é por mera conveniencia.
*
* 0: Processamento com sucesso
* -1: Código de serviço inválido
* -2: CEP de origem inválido
* -3: CEP de destino inválido
* -4: Peso excedido
* -5: O Valor Declarado não deve exceder R$ 10.000,00
* -6: Serviço indisponível para o trecho informado
* -7: O Valor Declarado é obrigatório para este serviço
* -8: Este serviço não aceita Mão Própria
* -9: Este serviço não aceita Aviso de Recebimento
* -10: Precificação indisponível para o trecho informado
* -11: Para definição do preço deverão ser informados, também, o comprimento, a largura e altura do objeto em centímetros (cm).
* -12: Comprimento inválido.
* -13: Largura inválida.
* -14: Altura inválida.
* -15: O comprimento não pode ser maior que 105 cm.
* -16: A largura não pode ser maior que 105 cm.
* -17: A altura não pode ser maior que 105 cm.
* -18: A altura não pode ser inferior a 2 cm.
* -20: A largura não pode ser inferior a 11 cm.
* -22: O comprimento não pode ser inferior a 16 cm.
* -23: A soma resultante do comprimento + largura + altura não deve superar a 200 cm.
* -24: Comprimento inválido.
* -25: Diâmetro inválido
* -26: Informe o comprimento.
* -27: Informe o diâmetro.
* -28: O comprimento não pode ser maior que 105 cm.
* -29: O diâmetro não pode ser maior que 91 cm.
* -30: O comprimento não pode ser inferior a 18 cm.
* -31: O diâmetro não pode ser inferior a 5 cm.
* -32: A soma resultante do comprimento + o dobro do diâmetro não deve superar a 200 cm.
* -33: Sistema temporariamente fora do ar. Favor tentar mais tarde.
* -34: Código Administrativo ou Senha inválidos.
* -35: Senha incorreta.
* -36: Cliente não possui contrato vigente com os Correios.
* -37: Cliente não possui serviço ativo em seu contrato.
* -38: Serviço indisponível para este código administrativo.
* -39: Peso excedido para o formato envelope
* -40: Para definicao do preco deverao ser informados, tambem, o comprimento e a largura e altura do objeto em centimetros (cm).
* -41: O comprimento nao pode ser maior que 60 cm.
* -42: O comprimento nao pode ser inferior a 16 cm.
* -43: A soma resultante do comprimento + largura nao deve superar a 120 cm.
* -44: A largura nao pode ser inferior a 11 cm.
* -45: A largura nao pode ser maior que 60 cm.
* -88: Erro ao calcular a tarifa
* 006: Localidade de origem não abrange o serviço informado
* 007: Localidade de destino não abrange o serviço informado
* 008: Serviço indisponível para o trecho informado
* 009: CEP inicial pertencente a área de Risco.
* 010: CEP final pertencente a área de Risco. A entrega será realizada, temporariamente, na agência mais próxima do endereço do destinatário.
* 011: CEP inicial e final pertencentes a área de Risco
* 7: Serviço indisponível, tente mais tarde
* 99: Outros erros diversos do .Net
*/
class Correios
{

    /**
     * Código administrativo junto à ECT.
     * O código está disponível no corpo do contrato firmado com os Correios.
     * @var string
     */
    private $__nCdEmpresa;

    /**
     * Senha para acesso ao serviço, associada ao código administrativo.
     * A senha inicial corresponde aos 8 primeiros dígitos do CNPJ informado no contrato.
     * A qualquer momento, é possível alterar a senha no endereço
     *
     * @see http://www.corporativo.correios.com.br/encomendas/servicosonline/recuperaSenha
     * @var string
     */
    private $__sDsSenha;

    /**
     * Códigos dos serviços prestados pelo Correios
     *
     * Lista de códigos:
     * 40010 - SEDEX Varejo
     * 40045 - SEDEX a Cobrar Varejo
     * 40215 - SEDEX 10 Varejo
     * 40290 - SEDEX Hoje Varejo
     * 41106 - PAC Varejo
     *
     * Pode ser mais de um numa consulta separados por vírgula.
     *
     * @var string
     */
    private $__nCdServico = '40010, 40045, 40215, 40290, 41106';

    /**
     * CEP de origem sem hifem
     * @example 05311900
     * @var integer
     */
    private $__sCepOrigem;

    /**
     * CEP de destino sem hifem
     * @example 05311900
     * @var integer
     */
    private $__sCepDestino;

    /**
     * Peso da encomenda, incluindo sua embalagem.
     * O peso deve ser informado em quilogramas.
     * Se o formato for Envelope, o valor máximo permitido será 1 kg.
     * @var integer
     */
    private $__nVlPeso;

    /**
     * Formato da encomenda (incluindo embalagem).
     * Valores possíveis: 1, 2 ou 3.
     *
     * 1 - Formato caixa/pacote
     * 2 - Formato rolo/prisma
     * 3 - Envelope
     *
     * @var integer
     */
    private $__nCdFormato = 1;

    /**
     * Comprimento da encomenda (incluindo embalagem), em centímetros.
     * @var float
     */
    private $__nVlComprimento;

    /**
     * Altura da encomenda (incluindo embalagem), em centímetros.
     * Se o formato for envelope, informar zero (0).
     * @var float
     */
    private $__nVlAltura;

    /**
     * Largura da encomenda (incluindo embalagem), em centímetros.
     * @var float
     */
    private $__nVlLargura;

    /**
     * Diâmetro da encomenda (incluindo embalagem), em centímetros.
     * @var float
     */
    private $__nVlDiametro = 0;

    /**
     * Indica se a encomenda será entregue com o serviço adicional mão própria.
     * Valores possíveis: S ou N (S – Sim, N – Não)
     *
     * @var string
     */
    private $__sCdMaoPropria = 'N';

    private $__StrRetorno = 'xml';

    private $__nIndicaCalculo = 3;

    /**
     * Indica se a encomenda será entregue com o serviço adicional valor declarado.
     * Neste campo deve ser apresentado o valor declarado desejado, em Reais.
     * Se não optar pelo serviço informar zero.
     * @var float
     */
    private $__nVlValorDeclarado = 0;

    /**
     * Indica se a encomenda será entregue com o serviço adicional aviso de recebimento.
     * Valores possíveis: S ou N (S – Sim, N – Não)
     * @var string
     */
    private $__sCdAvisoRecebimento = 'N';

    /**
     * Define o número do contrato que a empresa possui com o Correios
     * @param string $nCdEmpresa Número do contrato
     */
    public function setEmpresa($nCdEmpresa)
    {
        $this->__nCdEmpresa = (string)$nCdEmpresa;
        return $this;
    }

    /**
     * Define a senha específica da empresa com o Correios
     * @param string $sDsSenha Senha de acesso
     */
    public function setSenha($sDsSenha)
    {
        $this->__sDsSenha = (string)$sDsSenha;
        return $this;
    }

    /**
     * Define o número dos serviços dos quais serão utilizados no retorno, sedex, pac, etc
     * @param mixed $nCdServico Arranjo com os serviços
     */
    public function setServico($nCdServico)
    {

        // Um arranjo foi encaminhado?
        if ( is_array($nCdServico) ) $nCdServico = implode(', ', $nCdServico);

        // Define os serviços
        $this->__nCdServico = (string)$nCdServico;

        // retorna o objeto para trabalhar em método cadeia
        return $this;

    }

    /**
     * CEP de onde a mercadoria irá sair
     * @param integer $sCepOrigem Somente os números do CEP
     */
    public function setCepOrigem($sCepOrigem)
    {
        $this->__sCepOrigem = (integer)$sCepOrigem;
        return $this;
    }

    /**
     * CEP para onde a mercadoria irá ser enviada
     * @param integer $sCepDestino Somente números
     */
    public function setCepDestino($sCepDestino)
    {
        $this->__sCepDestino = (integer)$sCepDestino;
        return $this;
    }

    public function setPeso($nVlPeso)
    {
        $this->__nVlPeso = (integer)$nVlPeso;
        return $this;
    }

    public function setFormato($nCdFormato)
    {
        $this->__nCdFormato = (float)$nCdFormato;
        return $this;
    }

    public function setComprimento($nVlComprimento)
    {
        $this->__nVlComprimento = (float)$nVlComprimento;
        return $this;
    }

    public function setAltura($nVlAltura)
    {
        $this->__nVlAltura = (float)$nVlAltura;
        return $this;
    }

    public function setLargura($nVlLargura)
    {
        $this->__nVlLargura = (float)$nVlLargura;
        return $this;
    }

    public function setDiametro($nVlDiametro)
    {
        $this->__nVlDiametro = (float)$nVlDiametro;
        return $this;
    }

    public function setMaoPropria($sCdMaoPropria)
    {
        $this->__sCdMaoPropria = (string)$sCdMaoPropria;
        return $this;
    }

    public function setValorDeclarado($nVlValorDeclarado)
    {
        $this->__nVlValorDeclarado = (float)$nVlValorDeclarado;
        return $this;
    }

    public function setAvisoRecebimento($sCdAvisoRecebimento)
    {
        $this->__sCdAvisoRecebimento = (string)$sCdAvisoRecebimento;
        return $this;
    }

    /**
     * Realiza a requisição para o Web Service do Correios
     * @return object Com os erros ou com os dados da resposta
     */
    public function calcPrecoPrazo()
    {

        // Coleta todas a variáveis da classe
        $variables = get_object_vars($this);

        // Remove a variável com os erros (1ª da lista)
        array_shift($variables);

        // Monta o arranjo com as variáveis a serem utilizadas na URL da requisição
        foreach ($variables as $name => $value) $queryString[substr($name, 2)] = $value;

        // Monta o texto com os parâmetros
        $queryString = http_build_query($queryString);

        // Coleta o resultado
        $contents = file_get_contents("http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?{$queryString}");

        // Retorna o resultado da requisição
        return new SimpleXMLElement($contents);

    }

}

/* Fim do arquivo Correios.php */
/* Caminho: ./Correios.php */
?>